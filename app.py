from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import json

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = f"postgresql://{config['database']['user']}:{config['database']['password']}@{config['database']['host']}:{config['database']['port']}/{config['database']['name']}"
db = SQLAlchemy(app)

class Usuario(db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)
    cedula_identidad = db.Column(db.String(10), nullable=False)
    nombre = db.Column(db.String(50), nullable=False)
    primer_apellido = db.Column(db.String(50), nullable=False)
    segundo_apellido = db.Column(db.String(50))
    fecha_nacimiento = db.Column(db.Date, nullable=False)

@app.route('/usuarios', methods=['POST'])
def crear_usuario():
    data = request.get_json()
    nuevo_usuario = Usuario(**data)
    db.session.add(nuevo_usuario)
    db.session.commit()
    return jsonify({"message": "Usuario creado exitosamente"}), 201

@app.route('/usuarios', methods=['GET'])
def listar_usuarios():
    usuarios = Usuario.query.all()
    usuarios_json = [{"id": usuario.id,
                      "cedula_identidad": usuario.cedula_identidad,
                      "nombre": usuario.nombre,
                      "primer_apellido": usuario.primer_apellido,
                      "segundo_apellido": usuario.segundo_apellido,
                      "fecha_nacimiento": usuario.fecha_nacimiento.strftime("%Y-%m-%d")}
                     for usuario in usuarios]
    return jsonify(usuarios_json)

@app.route('/usuarios/<int:id_usuario>', methods=['GET'])
def obtener_usuario(id_usuario):
    usuario = Usuario.query.get(id_usuario)
    if usuario:
        usuario_json = {"id": usuario.id,
                        "cedula_identidad": usuario.cedula_identidad,
                        "nombre": usuario.nombre,
                        "primer_apellido": usuario.primer_apellido,
                        "segundo_apellido": usuario.segundo_apellido,
                        "fecha_nacimiento": usuario.fecha_nacimiento.strftime("%Y-%m-%d")}
        return jsonify(usuario_json)
    else:
        return jsonify({"message": "Usuario no encontrado"}), 404

@app.route('/usuarios/<int:id_usuario>', methods=['PUT'])
def actualizar_usuario(id_usuario):
    usuario = Usuario.query.get(id_usuario)
    if usuario:
        data = request.get_json()
        usuario.cedula_identidad = data.get('cedula_identidad', usuario.cedula_identidad)
        usuario.nombre = data.get('nombre', usuario.nombre)
        usuario.primer_apellido = data.get('primer_apellido', usuario.primer_apellido)
        usuario.segundo_apellido = data.get('segundo_apellido', usuario.segundo_apellido)
        usuario.fecha_nacimiento = data.get('fecha_nacimiento', usuario.fecha_nacimiento)
        db.session.commit()
        return jsonify({"message": "Usuario actualizado exitosamente"})
    else:
        return jsonify({"message": "Usuario no encontrado"}), 404

@app.route('/usuarios/<int:id_usuario>', methods=['DELETE'])
def eliminar_usuario(id_usuario):
    usuario = Usuario.query.get(id_usuario)
    if usuario:
        db.session.delete(usuario)
        db.session.commit()
        return jsonify({"message": "Usuario eliminado exitosamente"})
    else:
        return jsonify({"message": "Usuario no encontrado"}), 404


@app.route('/usuarios/promedio-edad', methods=['GET'])
def promedio_edad_usuarios():

    edades = db.session.query(db.func.age(db.func.current_date(), Usuario.fecha_nacimiento)).all()
    edades_float = [float(age[0].days) / 365 for age in edades if age[0] is not None]
    if edades_float:
        promedio = sum(edades_float) / len(edades_float)
        return jsonify({"promedioEdad": round(promedio, 2)})
    else:
        return jsonify({"message": "No hay usuarios en la base de datos"}), 404

@app.route('/estado', methods=['GET'])
def estado_api():

    info = {
        "nameSystem": "api-users",
        "version": "0.0.2",
        "developer": "Carlos David Montellano Barriga",
        "email": "montellano.carlos@usfx.bo"
    }
    return jsonify(info)
if __name__ == '__main__':
    app.run(debug=True)
